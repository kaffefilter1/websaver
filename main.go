package main

import (
	"bufio"
	"io"
	"log"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"
)

const (
	listenOn = ":3333"
)

type Server struct {
	wlock *sync.Mutex
	w     *bufio.Writer
}

func main() {
	log.Printf("Listening on %s", listenOn)

	f, err := os.OpenFile("stuff.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	// f, err := os.Create("stuff.txt")
	if err != nil {
		log.Fatal(err)
	}

	var s Server
	s.w = bufio.NewWriter(f)
	s.wlock = &sync.Mutex{}

	shutdown := make(chan os.Signal)
	signal.Notify(shutdown, syscall.SIGINT)
	go func() {
		log.Println("Waiting for TERM")
		<-shutdown
		s.wlock.Lock()
		log.Println("Closing")
		s.Close()
		f.Close()
		os.Exit(0)
	}()

	http.HandleFunc("/append", s.webAppend)
	http.HandleFunc("/flush", s.webFlush)

	log.Fatal(http.ListenAndServe(listenOn, nil))
}

func (s *Server) webAppend(w http.ResponseWriter, r *http.Request) {

	// Open the lock
	s.wlock.Lock()
	// Just write it
	_, err := io.Copy(s.w, r.Body)
	if err != nil {
		log.Printf("error: %s", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	s.wlock.Unlock()

	return
}

func (s *Server) webFlush(w http.ResponseWriter, r *http.Request) {
	s.wlock.Lock()
	if err := s.w.Flush(); err != nil {
		log.Printf("error: %s", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
	}

	s.wlock.Unlock()

}

func (s *Server) Close() {
	if err := s.w.Flush(); err != nil {
		log.Fatal(err)
	}
}

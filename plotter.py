#!/usr/bin/env python3

import matplotlib.pyplot as plt
import csv
import sys
import datetime

latencyminus = 7200
latenccolor = "tab:red"
pacccolor = "tab:blue"
perseccolor = "tab:green"

base = "2020-05-23T"
timezone = 2
changes = {
        "19:20:56": 7,
        "19:30:25": 20,
        "19:31:31": 50,
        "19:32:31": 100,
        "19:35:31": 150,
        "19:37:19": 200,
        "19:40:33": 225
}


def start(thing):
    print(thing, " ", end="")
    sys.stdout.flush()
def done(thing=""):
    print("done ", thing, " ")
    sys.stdout.flush()

def add(dict, key, val):
    if val < 0:
        val = 0
    if key in dict:
        dict[key] += val
    else:
        dict[key] = val

# data = {}
total = {}
count = 0
countpersec = {}

start("loading dataset")
with open("stuff.txt", "r") as f:
    reader = csv.reader(f, delimiter=",")
    for row in reader:
        if len(row) < 3:
            continue

        count += 1
        # add(data, row[0], [row[2] - latencyminus])
        add(total, int(row[0]), int(row[2]) - latencyminus)
        add(countpersec, int(row[0]), 1)

done(count)

# Convert to unix time
paccdata = {}
for change, value in changes.items():
    dt = datetime.datetime.strptime(base + change, "%Y-%m-%dT%H:%M:%S")
    paccdata[int(dt.timestamp())] = value

start("calculating start time")

starttime = min(list(total.keys()) + list(paccdata.keys()))
done()

start("processing data")

newpaccdata = {}
for key in paccdata.keys():
    newpaccdata[key - starttime] = paccdata[key]
paccdata = newpaccdata

average = {}
for key in total.keys():
    average[key - starttime] = total[key] / countpersec[key]

new = {}
for key in countpersec.keys():
    new[key - starttime] = countpersec[key]
countpersec = new
done()

start("creating plot")
fig, latenc = plt.subplots()
fig.subplots_adjust(right=0.75)
latenc.set_xlabel("time [s]")
latenc.set_ylabel("latency [s]", color=latenccolor)
latenc.bar(average.keys(), average.values(), color=latenccolor)
latenc.tick_params(axis="y", labelcolor=latenccolor)

pacc = latenc.twinx()
pacc.set_ylabel("Number of simulated PACs", color=pacccolor)
pacc.step(list(paccdata.keys()), list(paccdata.values()))
pacc.tick_params(axis="y", labelcolor=pacccolor)

persec = latenc.twinx()
persec.set_ylabel("Number of spot updates per second", color=perseccolor)
persec.scatter(list(countpersec.keys()), list(countpersec.values()), color=perseccolor, alpha=0.5,marker='.')
persec.tick_params(axis="y", labelcolor=perseccolor)
persec.spines["right"].set_position(("axes", 1.2))
persec.spines["right"].set_visible(True)
done()

start("saving plot")
plt.savefig("plot.pdf")
done()
